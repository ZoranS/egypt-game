﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cactus : MonoBehaviour
{
    private void Start()
    {
        Debug.Log("Player Health = " + Player.Health);
    }

    private void Update()
    {
        if (Player.Health <= 0)
        {
            Debug.Log("Player is dead \nRIP");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Player.Health -= 1;

            Debug.Log(other.gameObject.name + " Raakte " + this.gameObject.name + " aan");
            Debug.Log("Player Health = " + Player.Health);
        }
    }
}
